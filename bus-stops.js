// https://www.codewars.com/kata/number-of-people-in-the-bus/

let number = (busStops) => busStops.reduce((rem, [on, off]) => rem + on - off, 0)