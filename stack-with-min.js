/* 
How would you design a stack which, besides, to push and pop, also has a function min which return the minimum element? Push, pop and min should all operate in O(1) time.
*/

class StackWithMin {
  constructor() {
    this.items = [];
    this.mins = [];
  }
  push(value){
    if (value <= this.min()) {
      this.mins.push(value);
    }
    this.items.push(value);
  }
  pop() {
    const value = this.items.pop();
    if (value === this.min()) {
      this.mins.pop();
    }
    return value;
  }
  min() {
    if (this.mins.length === 0) {
      return Number.MAX_SAFE_INTEGER;
    } else {
      return this.mins[this.mins.length - 1];
    }
  }
}