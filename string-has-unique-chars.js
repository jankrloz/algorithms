/*
Implement an algorithm to determine if a string has all unique characters.
*/

function isUniqueChars(str) {
  if (str.length > 256)
    return false;
  
  const char_set = [];
  for(let i = 0; i < str.length; i++) {
    const val = str[i];
    if(char_set[val]) {
      return false;
    }
    char_set[val] = true;
  }
  return true;
}