// https://www.codewars.com/kata/5287e858c6b5a9678200083c

function narcissistic(value) {
  value = value.toString();
  let len = value.length;
  
  let sum = 0;

  for (let i = 0; i < len; i++) {
    sum += parseInt(Math.pow(value[i], len));
  }
  
  return sum === parseInt(value);
}
