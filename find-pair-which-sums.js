/*
Design an algorithm to find pairs of integers within an array which sum to a specified value.
*/

function findPairs(arr, sum) {
  const set = new Set();
  arr.forEach(x => {
    if( set.has(sum - x) ) {
      console.log(`${x} - ${sum - x}`);
    }
    set.add(x);
  });
}

// findPairs([1, 2, 3, 4, 5], 5);