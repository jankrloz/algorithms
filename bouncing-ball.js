// https://www.codewars.com/kata/bouncing-balls/javascript

function bouncingBall(h,  bounce,  window) {
  if (h > 0 && (0 < bounce && bounce < 1) && window < h) {
    let seeing = -1; // First falling doesn't have previous bounce 
    while (h > window) {
      seeing += 2; // Bouncing and falling
      h *= bounce;
    }
    return seeing;
  } else return -1
}